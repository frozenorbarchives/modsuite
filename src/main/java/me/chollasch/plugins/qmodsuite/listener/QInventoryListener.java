package me.chollasch.plugins.qmodsuite.listener;

import com.google.common.collect.Lists;
import me.chollasch.plugins.qmodsuite.Config;
import me.chollasch.plugins.qmodsuite.QModSuite;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Horse;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static me.chollasch.plugins.qmodsuite.inventory.StaffItems.build;

/**
 * Created by Connor Hollasch on 10/2/14.
 */

public class QInventoryListener implements Listener {
    public static final long UPDATE_DELAY = 12L;

    private static HashMap<String, Inventory> customInvs = new HashMap<>();

    public QInventoryListener(){
        //TODO - Auto inventory updater
        new BukkitRunnable(){
            @Override
            public void run(){
                for(String name : customInvs.keySet()){
                    Inventory inv = customInvs.get(name);
                    Player player = Bukkit.getPlayerExact(name);

                    if(player == null){
                        //Just notify the viewers
                        //TODO - Spam prevention?
                        for(HumanEntity entity : inv.getViewers()){
                            //((Player) entity).sendMessage(ChatColor.RED + "" + ChatColor.BOLD + name + " has logged out.");
                        }
                    } else {
                        updateInventory(player, inv);
                    }
                }
            }
        }.runTaskTimer(QModSuite.getInstance(), UPDATE_DELAY, UPDATE_DELAY);
    }

    private static void updateInventory(Player player, Inventory inv){
        //First, clear the inventory
        inv.clear();

        //Fill the inventory with special view items
        //Mark the start of the inventory for special items
        int start = 36;

        //Fill the inventory with the players armor (first few slots)
        inv.setItem(start++, player.getEquipment().getHelmet());
        inv.setItem(start++, player.getEquipment().getChestplate());
        inv.setItem(start++, player.getEquipment().getLeggings());
        inv.setItem(start++, player.getEquipment().getBoots());

        //Set the next slot with the players current item
        inv.setItem(start++, player.getItemInHand().clone());

        //Fill empty slots with stained glass spacers
        ItemStack spacer = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 14);
        for (int i = 0; i < 4; i++) {
            inv.setItem(start++, spacer);
        }

        //Show the players health with a speckled melon
        inv.setItem(start++, build(Material.SPECKLED_MELON,
                (int) player.getHealth(),
                Config.get().getString("DEFAULTS.GUI.HEALTH-DISPLAY").replace("{0}", (int) player.getHealth() + "").replace("{1}", (int) player.getMaxHealth() + "")));

        //Show the players hunger
        inv.setItem(start++, build(Material.COOKED_BEEF,
                player.getFoodLevel(),
                Config.get().getString("DEFAULTS.GUI.HUNGER-DISPLAY").replace("{0}", player.getFoodLevel() + "")));

        //Show the players saturation
        inv.setItem(start++, build(Material.ROTTEN_FLESH,
                (int) player.getSaturation(),
                Config.get().getString("DEFAULTS.GUI.SATURATION-DISPLAY").replace("{0}", player.getSaturation() + "")));

        //Show if the player is on a horse
        if (player.getVehicle() != null && player.getVehicle() instanceof Horse) {
            inv.setItem(start++, build(Material.DIAMOND_BARDING, Config.get().getString("DEFAULTS.GUI.RIDING-HORSE")));
        } else {
            inv.setItem(start++, build(Material.IRON_BARDING, Config.get().getString("DEFAULTS.GUI.NO-HORSE")));
        }

        //Display the users current potion effects
        Collection<PotionEffect> effects = player.getActivePotionEffects();
        ItemStack pots = build((effects.size() == 0 ? Material.GLASS_BOTTLE : Material.POTION), Config.get().getString("DEFAULTS.GUI.POTION-EFFECTS"));

        //Setup lore
        ItemMeta potsMeta = pots.getItemMeta();
        List<String> lore = Lists.newArrayList();

        //Add potion effects to the players lore
        for (String pot : formatEffects(effects)){
            lore.add(pot);
        }

        potsMeta.setLore(lore);
        pots.setItemMeta(potsMeta);

        inv.setItem(start++, pots);

        //Display the xp level
        inv.setItem(start++, build(Material.EXP_BOTTLE, player.getLevel(),
                Config.get().getString("DEFAULTS.GUI.EXP").replace("{0}", player.getLevel() + "")));

        //Display targets gamemode
        inv.setItem(start++, build(Material.WOOD_PICKAXE,
                1, Config.get().getString("DEFAULTS.GUI.CURRENT-GAMEMODE").replace("{0}", player.getGameMode().name())));

        //Display targets location
        inv.setItem(start++, build(Material.COMPASS,
                1, Config.get().getString("DEFAULTS.GUI.CURRENT-LOCATION").replace("{0}", player.getLocation().getBlockX() + "")
                        .replace("{1}", player.getLocation().getBlockY() + "")
                        .replace("{2}", player.getLocation().getBlockZ() + "")));

        //Display the modify inventory chest
        inv.setItem(start++, build(Material.CHEST,
                1, Config.get().getString("DEFAULTS.GUI.MODIFY-INVENTORY")));

        //Fill the inventory with the targets inventory contents
        for (int i = 0; i < player.getInventory().getSize(); i++) {
            inv.setItem(i, player.getInventory().getContents()[i]);
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        Inventory inv = event.getInventory();
        Player player = (Player) event.getWhoClicked();

        if(!(customInvs.containsValue(inv))){
            return;
        }

        //Cancel any click events!
        event.setCancelled(true);

        //Check if the clicked slot is that last item (inventory editor)
        if(event.getSlot() == inv.getSize() - 1){
            //Modify inv
            if (player.hasPermission(Config.get().getString("DEFAULTS.PERMISSION.MODIFY-INVENTORY"))) {
                //Allow inventory modification
                //Open the targets inventory
                for(String name : customInvs.keySet()){
                    if(customInvs.get(name).equals(inv)){
                        Player target = Bukkit.getPlayerExact(name);

                        if(target != null){
                            player.openInventory(target.getInventory());
                        } else {
                            player.sendMessage(ChatColor.RED + name + " is not online anymore!");
                        }

                        break;
                    }
                }

                //Close the custom inventory
                //close(player);
            } else {
                //Deny
                return;
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        /*
        //Be sure to close the inventory if it's a custom open!
        if (openInvs.containsKey(event.getPlayer())) {
            //Close with the close method!
            close((Player)event.getPlayer());
        }
        */
    }

    public static void open(Player user, Player target){
        //Check for it in the cache
        if(customInvs.containsKey(target.getName())){
            user.openInventory(customInvs.get(target.getName()));
        } else {
            //Create the inventory
            Inventory inv = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', Config.get().getString("DEFAULTS.EXAM-INVENTORY-TITLE").replace("{0}", target.getName())));

            updateInventory(target, inv);
            customInvs.put(target.getName(), inv);
            user.openInventory(inv);
        }

        /*
        //Create the inventory
        Inventory create = Bukkit.createInventory(user, 54, ChatColor.translateAlternateColorCodes('&', Config.get().getString("DEFAULTS.EXAM-INVENTORY-TITLE").replace("{0}", target.getName())));

        //Update / set items in the inventory
        user.openInventory(create);
        update(user, target, create);

        //Put the player in the open inventories hash map
        openInvs.put(user, create);
        targets.put(user, target);
        */
    }

    private static String[] formatEffects(Collection<PotionEffect> activePotionEffects) {
        String[] x = new String[activePotionEffects.size()];

        int pos = -1;
        for (PotionEffect effect : activePotionEffects) {
            x[++pos] = ChatColor.RESET + effect.getType().getName()+" "+(effect.getDuration()/20) + "s";
        }

        return x;
    }
}
