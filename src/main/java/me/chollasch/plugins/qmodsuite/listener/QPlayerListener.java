package me.chollasch.plugins.qmodsuite.listener;

import me.chollasch.plugins.qmodsuite.Config;
import me.chollasch.plugins.qmodsuite.QModSuite;
import me.chollasch.plugins.qmodsuite.inventory.InventoryLogger;
import me.chollasch.plugins.qmodsuite.inventory.StaffItems;
import me.chollasch.plugins.qmodsuite.message.Language;
import me.chollasch.plugins.qmodsuite.task.RecordingTask;
import me.chollasch.plugins.qmodsuite.user.StaffUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.io.IOException;

/**
 * Created by Connor Hollasch on 9/29/14.
 */

public class QPlayerListener implements Listener {

    //==================================================================================================================

    /**
     * Generic normal priority listener for player join events.
     *
     * @param event event fired when a player joins
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) throws IOException {
        Player player = event.getPlayer();
        //Make sure we can't see any hidden staff members
        for (Player hidden : QModSuite.getHiddenPlayers()) {
            player.hidePlayer(hidden);
        }

        //Create a StaffPlayer object for the player
        StaffUser user = new StaffUser(player);

        //Validate the player has the permission for the mod suite, otherwise we don't care about the player anymore!
        if (!(player.hasPermission(Config.get().getString("DEFAULTS.PERMISSION.STAFF-MODE"))))
            return;

        //Try and load the players data from crashed server!
        if (InventoryLogger.containsPlayer(player))
            InventoryLogger.loadPlayerData(player);

        //Validate staff mode is default true
        if (Config.get().getBoolean("DEFAULTS.JOIN-IN-STAFF-MODE")) {
            //Join in staff mode is set to true, so we put the player in the staff mode!

            //Send the player a message saying they're in staff mode
            Language.PLAYER_JOIN_NOTIFY.send(player);

            //Set staff mode to true and update
            user.setIsStaffMode(true);
        }
    }

    //==================================================================================================================

    /**
     * Called when a player right / left clicks with their mouse.
     * Event is also fired upon player interacting with an item specifically (pressure plate, button, etc).
     *
     * @param event mouse click event
     * @throws IOException
     */
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) throws IOException {
        //Retrieve the player from the listener
        Player player = event.getPlayer();

        //Short circuit eval, make sure they're IN staff mode
        if (!(StaffUser.getUserFromPlayer(player).isStaffMode()))
            return;

        //Retrieve the action from the listener
        Action action = event.getAction();

        //Validate the player right clicked
        if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {

            //Get the stack the player is holding
            ItemStack holding = event.getItem();

            //Nullcheck
            if (holding == null)
                return;

            if (holding.equals(StaffItems.RANDOM_TELEPORT)) {
                //Get a random user to teleport to
                Player tpTo = getRandomPlayer(player, 0);
                player.teleport(tpTo);

                //Notify the player who they teleported to
                Language.RANDOM_TELEPORT.send(player, tpTo.getName());
            } else if (holding.equals(StaffItems.RECORDING_OFF)) {
                //Toggle recording on
                player.getInventory().setItem(3, StaffItems.RECORDING_ON);

                //Notify the player recording is on
                Language.RECORDING_ON.send(player);

                //Add the player to the recording task list
                RecordingTask.addPlayerToRecordingList(player);

                for (Player other : Bukkit.getOnlinePlayers()) {
                    player.hidePlayer(other);
                }
            } else if (holding.equals(StaffItems.RECORDING_ON)) {
                //Notify the player recording is on
                Language.RECORDING_OFF.send(player);

                //Add the player to the recording task list
                RecordingTask.removePlayerFromRecordingTask(player);
                player.removePotionEffect(PotionEffectType.INVISIBILITY);

                //Reset player visibility and player items
                StaffUser.getUserFromPlayer(player).updateStaffMode();
            }
        }
    }

    private Player getRandomPlayer(Player ignore, int tries) {
        Player players[] = Bukkit.getOnlinePlayers();

        if(players.length == 1){
            return players[0];
        } else if(players.length == 0){
            return null;
        }

        Player tpTo = players[(int) (Math.random() * players.length)];

        if(tpTo.equals(ignore)){
            if(tries > 50){
                return tpTo;
            }

            return getRandomPlayer(ignore, tries++);
        }

        return tpTo;
    }

    //==================================================================================================================

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        //Get the player from the interact event
        Player player = event.getPlayer();

        //Short circuit eval, make sure they're IN staff mode
        if (!(StaffUser.getUserFromPlayer(player).isStaffMode()))
            return;

        //Get right clicked entity
        Entity interacted = event.getRightClicked();

        //Make sure the interacted entity is a player and teh player is holding the inventory viewer
        if (player.getItemInHand().equals(StaffItems.PGUI_BOOK) && (interacted instanceof Player)) {
            //Cast to player for interacted
            Player clicked = (Player)interacted;

            //TODO add inventory viewing
            QInventoryListener.open(player, clicked);
        }
    }

    //==================================================================================================================

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (StaffUser.getUserFromPlayer(event.getPlayer()).isStaffMode())
            event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            if (StaffUser.getUserFromPlayer((Player)event.getEntity()).isStaffMode())
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDamagePlayer(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            if (StaffUser.getUserFromPlayer((Player)event.getDamager()).isStaffMode())
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (StaffUser.getUserFromPlayer(event.getPlayer()).isStaffMode())
            event.setCancelled(true);
    }
}
