package me.chollasch.plugins.qmodsuite.message;

import me.chollasch.plugins.qmodsuite.Config;
import me.chollasch.plugins.qmodsuite.QModSuite;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Connor Hollasch on 9/29/14.
 */

public enum Language {

    //==================================================================================================================

    NO_PERMISSION("NO-PERMISSION"),

    STAFF_MODE_OFF("TOGGLE-OFF"),
    STAFF_MODE_ON("TOGGLE-ON"),

    RANDOM_TELEPORT("RANDOM-TELEPORT"),

    RECORDING_ON("RECORDING-ON"),
    RECORDING_OFF("RECORDING-OFF"),

    PLAYER_JOIN_NOTIFY("PLAYER-JOIN-NOTIFY");

    //==================================================================================================================

    /** Represents the string version of the language message */
    private String message;

    /** Represents a list of the messages */
    private List<String> messageList;

    //==================================================================================================================

    /**
     * Construct a language value with the given configuration section.
     * Loads the string / string list from the parameter section.
     *
     * @param section configuration section of the message / language option
     */
    private Language(String section) {
        QModSuite suite = QModSuite.getInstance();
        System.out.println(suite.getConfig().get("MESSAGES."+section));
        if (suite.getConfig().isList("MESSAGES."+section))
            messageList = suite.getConfig().getStringList("MESSAGES."+section);
        else
            message = suite.getConfig().getString("MESSAGES."+section);
    }

    //==================================================================================================================

    /**
     * Send a message to the given player with the given language section and arguments to format.
     *
     * @param player player to send the message to
     * @param args arguments to replace in the message / messages
     */
    public void send(Player player, String... args) {
        if (message == null) {
            //The language option is a list of messages
            for (String message : messageList) {
                send(player, message, args);
            }

            return;
        }
        if (args.length == 0) {
            player.sendMessage(tacc(message));
            return;
        }

        String format = tacc(message);

        int current = -1;
        for (String arg : args) {
            format = format.replace("{"+(current++)+"}", arg);
        }
    }

    /**
     * Send the message / messages to the whole server.
     *
     * @param args arguments to replace in the message / messages
     */
    public void send(String... args) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            send(player, args);
        }
    }

    /**
     * Sends the physical message to the player and formats it.
     *
     * @param player player to send the message to
     * @param raw raw message to format and send
     * @param args arguments to replace in the message
     */
    private void send(Player player, String raw, String... args) {
        //Format the message color codes
        String format = tacc(raw);

        //Iterate through each argument and replace {argnum} with the physical argument
        int current = -1;
        for (String arg : args) {
            format = format.replace("{" + (current++) + "}", arg);
        }

        //Send the message to the player
        player.sendMessage(format);
    }

    //==================================================================================================================

    /**
     * Translate alternate color codes shortcut.
     *
     * @see org.bukkit.ChatColor
     * @param input string to format
     * @return formatted string
     */
    private static String tacc(String input) {
        return ChatColor.translateAlternateColorCodes('&', input);
    }
}
