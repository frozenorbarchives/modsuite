package me.chollasch.plugins.qmodsuite;

import com.google.common.collect.Lists;
import me.chollasch.plugins.qmodsuite.inventory.InventoryLogger;
import me.chollasch.plugins.qmodsuite.inventory.StaffItems;
import me.chollasch.plugins.qmodsuite.listener.QInventoryListener;
import me.chollasch.plugins.qmodsuite.listener.QPlayerListener;
import me.chollasch.plugins.qmodsuite.message.Language;
import me.chollasch.plugins.qmodsuite.task.RecordingTask;
import me.chollasch.plugins.qmodsuite.user.StaffUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.List;

/**
 * Created by Connor Hollasch on 9/29/14.
 */

public class QModSuite extends JavaPlugin {

    //==================================================================================================================

    /** Static access to the QModSuite plugin */
    private static QModSuite instance;

    /** Hidden staff members who are in staff mode with vanish */
    private static List<Player> hidden = Lists.newArrayList();

    //==================================================================================================================

    /**
     * Called when the plugin is loaded.
     */
    public void onEnable() {
        //Assign the QModSuite instance
        instance = this;

        //Save the config
        saveDefaultConfig();

        //Initialize the inventory logger
        try {
            InventoryLogger.init();
        } catch (IOException e) {
            //Print the IOException thrown
            e.printStackTrace();
        }

        //Load the plugin items
        StaffItems.load();

        //Load the listeners
        QPlayerListener playerListener = new QPlayerListener();
        Bukkit.getPluginManager().registerEvents(playerListener, this);
        Bukkit.getPluginManager().registerEvents(new QInventoryListener(), this);

        //Call join for online players
        for (Player player : Bukkit.getOnlinePlayers()) {
            try {
                playerListener.onPlayerJoin(new PlayerJoinEvent(player, null));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //Schedule tasks
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new RecordingTask(), 20*5, 20*5);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new RecordingTask(), 20*3, 20*3);
    }

    /**
     * Called when the plugin is unloaded.
     */
    public void onDisable() {
        //Save the config (for any changes)
        saveDefaultConfig();
    }

    //==================================================================================================================

    /**
     * Get the plugin instance.
     *
     * @return plugin instance
     */
    public static QModSuite getInstance() {
        return instance;
    }

    /**
     * Get the hidden player list.
     *
     * @return list of hidden players
     */
    public static List<Player> getHiddenPlayers() {
        return hidden;
    }

    //==================================================================================================================

    /**
     * Called when someone runs a command (player or console sender).
     *
     * @param sender sender of the command
     * @param command command executed by the sender
     * @param label physical typed label of the command
     * @param args arguments specified after the command
     * @return if successful
     */
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //Command senders must be players
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
            return true;
        }

        Player player = (Player)sender;

        //Check if the player can use the command
        if (!(player.hasPermission(Config.get().getString("DEFAULTS.PERMISSION.STAFF-MODE")))) {
            //No permission!
            Language.NO_PERMISSION.send(player);
            return true;
        }

        //Get staff user from player
        StaffUser user = StaffUser.getUserFromPlayer(player);

        if (user.isStaffMode()) {
            //Get them out of staff mode
            user.setIsStaffMode(false);
            Language.STAFF_MODE_OFF.send(player);
        } else {
            //Set them into staff mode
            user.setIsStaffMode(true);
            Language.STAFF_MODE_ON.send(player);
        }

        return true;
    }

    //==================================================================================================================
}
