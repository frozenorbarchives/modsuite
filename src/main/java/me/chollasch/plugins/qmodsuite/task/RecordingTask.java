package me.chollasch.plugins.qmodsuite.task;

import com.google.common.collect.Lists;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

/**
 * Created by Connor Hollasch on 10/1/14.
 */

public class RecordingTask implements Runnable {

    /** Holds all the players who are recording */
    private static List<Player> recording = Lists.newArrayList();

    /**
     * Called every 5 seconds to update the effects
     */
    public void run() {
        //Add invisibility to all players in the recording list
        for (Player player : recording) {
            player.addPotionEffect(PotionEffectType.INVISIBILITY.createEffect(Integer.MAX_VALUE, 0));
        }
    }

    /**
     * Add a player to the recording list.
     *
     * @param player player to add to recording list
     */
    public static void addPlayerToRecordingList(Player player) {
        recording.add(player);
    }

    /**
     * Remove a player from the recording list.
     *
     * @param player player to remove from the list
     */
    public static void removePlayerFromRecordingTask(Player player) {
        recording.remove(player);
    }

    /**
     * Check if the specified player is recording or not.
     *
     * @param player player to check
     * @return if the player is recording
     */
    public static boolean isRecording(Player player) {
        return recording.contains(player);
    }
}
