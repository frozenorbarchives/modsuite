package me.chollasch.plugins.qmodsuite.inventory;

import me.chollasch.plugins.qmodsuite.QModSuite;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.*;
import java.util.HashMap;

/**
 * Created by Connor Hollasch on 9/29/14.
 */

public class InventoryLogger {

    /** Map of all players saved contents (not armor) from their inventory */
    private static HashMap<String, ItemStack[]> savedContents = new HashMap<>();

    /** Map of all players saved armor contents from their current build */
    private static HashMap<String, ItemStack[]> savedArmor = new HashMap<>();

    /** Represents the backup file for saving / loading inventories */
    private static File backupFile = new File(QModSuite.getInstance().getDataFolder(), "inventories.save");

    /** Writer / reader for the backup file */
    private static BufferedWriter writer;
    private static BufferedReader reader;

    public static void init() throws IOException {
        //Check if the plugin folder exists, if it doesn't create it
        if (!(QModSuite.getInstance().getDataFolder().exists()))
            QModSuite.getInstance().getDataFolder().mkdirs();

        //Make sure the file actually exists, otherwise create it too
        if (!(backupFile.exists()))
            backupFile.createNewFile();

        //Initialize the reader and writer
        reader = new BufferedReader(new FileReader(backupFile));

        String line;
        while ((line = reader.readLine()) != null) {
            String player = line.substring(0, line.indexOf("|"));
            Inventory load = InventoryIO.deserializeInventory(line.substring(line.indexOf("|") + 1));

            System.out.println(player+", "+load.getContents());

            //Put the lost data into the map for restoration
            savedContents.put(player, load.getContents());
        }

        reader = new BufferedReader(new FileReader(backupFile));
    }

    public static boolean containsPlayer(Player player) {
        return savedContents.containsKey(player.getName());
    }

    public static void savePlayerData(Player player) throws IOException {
        writer = new BufferedWriter(new FileWriter(backupFile));
        reader = new BufferedReader(new FileReader(backupFile));

        if (savedContents.containsKey(player.getName())) {
            savedContents.remove(player.getName());
            savedArmor.remove(player.getName());
        }

        savedContents.put(player.getName(), player.getInventory().getContents());
        savedArmor.put(player.getName(), player.getInventory().getArmorContents());

        String cache = InventoryIO.serializeInventory(player.getInventory());

        //Make sure we save the players inventory just in case!
        new PrintWriter(writer).println(player.getName() + "|" + cache);
        writer.close();
    }

    /**
     * Used to remove a given line in a file that starts / equals with the given string.
     *
     * @param start string that you want to remove
     * @throws IOException
     */
    private static void removeLine(String start) throws IOException {
        writer = new BufferedWriter(new FileWriter(backupFile));
        reader = new BufferedReader(new FileReader(backupFile));

        //Declare a string to represent the current line in a file
        String line;
        while ((line = reader.readLine()) != null) {
            //Check if the trimmed string starts with the line we want to remove, if it does, keep going (don't add)
            String trim = line.trim();
            if (trim.startsWith(start))
                continue;

            //Write the line if we don't want to remove it
            writer.write(line);
        }
    }

    /**
     * Used to load a players inventory.
     *
     * @param player to load inventory onto
     */
    public static void loadPlayerData(Player player) throws IOException {
        //Clear the players inventory so we can load the data
        player.getInventory().clear();

        try {
            player.getInventory().setContents(savedContents.remove(player.getName()));

            //We might not have the inventory stored in the armor contents
            if (savedArmor.containsKey(player.getName()))
                player.getInventory().setArmorContents(savedArmor.remove(player.getName()));
        } catch (NullPointerException ex) {
            //Inventory saved was empty!
        }

        //Remove player from file
        removeLine(player.getName());

        //Update their inventory
        player.updateInventory();
    }
}
