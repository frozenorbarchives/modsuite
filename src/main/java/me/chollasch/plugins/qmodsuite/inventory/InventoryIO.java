package me.chollasch.plugins.qmodsuite.inventory;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Connor Hollasch on 9/29/14.
 */

public class InventoryIO {

    //==================================================================================================================

    /**
     * Serialize an inventory into a string format for fast item saving.
     *
     * @param invInventory inventory to conver to a string
     * @return string version fo the given inventory
     */
    public static String serializeInventory(Inventory invInventory) {
        //Begin serialization
        String serialization = invInventory.getSize() + ";";

        //Iterate through each item in the inventory
        for (int i = 0; i < invInventory.getSize(); i++) {

            //Get the current item stack in the inventory
            ItemStack is = invInventory.getItem(i);
            //Make sure the item isn't null, otherwise, keep going
            if (is != null) {
                //Create an empty string to insert the inventory save into
                String serializedItemStack = new String();

                //Get the type of the inventory
                String isType = String.valueOf(is.getType().getId());
                serializedItemStack += "t@" + isType;

                //Check if the durability isn't 0, if so, then save it
                if (is.getDurability() != 0) {
                    String isDurability = String.valueOf(is.getDurability());
                    serializedItemStack += ":d@" + isDurability;
                }

                //Check if the amount is more than 1, if so, then save it!
                if (is.getAmount() != 1) {
                    String isAmount = String.valueOf(is.getAmount());
                    serializedItemStack += ":a@" + isAmount;
                }

                //Save display name if it's not null
                if (is.getItemMeta().getDisplayName() != null) {
                    String display = is.getItemMeta().getDisplayName();
                    serializedItemStack += ":n@" + display;
                }

                //Save the lore if the item has one (not null)
                if (is.getItemMeta().getLore() != null) {
                    List<String> lore = is.getItemMeta().getLore();
                    for (String l : lore) {
                        serializedItemStack += ":l@" + l;
                    }
                }

                //Get the map of all the items enchantments
                Map<Enchantment, Integer> isEnch = is.getEnchantments();

                //If there are more than 0 enchantments, save them
                if (isEnch.size() > 0) {
                    for (Map.Entry<Enchantment, Integer> ench : isEnch.entrySet()) {
                        serializedItemStack += ":e@" + ench.getKey().getId() + "@" + ench.getValue();
                    }
                }

                //Add the serialized item to the string
                serialization += i + "#" + serializedItemStack + ";";
            }
        }

        //Return the serialized item
        return serialization;
    }

    //==================================================================================================================

    /**
     * Deserializer an inventory from a given string.
     * Preserve format outputted from intToString.
     *
     * @param invString string input to convert to an inventory
     * @return inventory representation of the string
     */
    public static Inventory deserializeInventory(String invString) {
        //Split all of the ; characters to split data
        String[] serializedBlocks = invString.split(";");
        String invInfo = serializedBlocks[0];
        //Create an inventory with the first thing of data as inventory size
        Inventory deserializedInventory = Bukkit.getServer().createInventory(null, Integer.valueOf(invInfo));

        //Iterate through each item in the inventory
        for (int i = 1; i < serializedBlocks.length; i++) {
            //Split across the # character to get the specific item data
            String[] serializedBlock = serializedBlocks[i].split("#");

            //Get the location in the inventory of the specific item
            int stackPosition = Integer.valueOf(serializedBlock[0]);

            //Check if the location of the item is greater than the size,
            //if it is, keep going through the items as it is not possible
            //for an item to go out of an inventory
            if (stackPosition >= deserializedInventory.getSize()) {
                continue;
            }

            //Initialize item dependent variables
            ItemStack is = null;
            Boolean createdItemStack = false;

            //Load the data for the item (type, amount, data, name, etc)
            String[] serializedItemStack = serializedBlock[1].split(":");

            //Iterate through the item options
            for (String itemInfo : serializedItemStack) {
                String[] itemAttribute = itemInfo.split("@");
                if (itemAttribute[0].equals("t")) { //Check for the type of the item
                    is = new ItemStack(Material.getMaterial(Integer.valueOf(itemAttribute[1])));
                    createdItemStack = true;
                } else if (itemAttribute[0].equals("d") && createdItemStack) { //Check for durability
                    is.setDurability(Short.valueOf(itemAttribute[1]));
                } else if (itemAttribute[0].equals("a") && createdItemStack) { //Check for amount
                    is.setAmount(Integer.valueOf(itemAttribute[1]));
                } else if (itemAttribute[0].equals("e") && createdItemStack) { //Check for enchantments
                    is.addUnsafeEnchantment(Enchantment.getById(Integer.valueOf(itemAttribute[1])), Integer.valueOf(itemAttribute[2]));
                } else if (itemAttribute[0].equals("n") && createdItemStack) { //Check for item name
                    ItemMeta m = is.getItemMeta();
                    m.setDisplayName(itemAttribute[1]);
                    is.setItemMeta(m);
                } else if (itemAttribute[0].equals("l") && createdItemStack) { //Check for item lore
                    ItemMeta m = is.getItemMeta();
                    List<String> cLore = m.getLore();
                    if (cLore == null)
                        cLore = new ArrayList<>();
                    cLore.add(itemAttribute[1]);
                    m.setLore(cLore);
                    is.setItemMeta(m);
                }
            }

            //Set the item in the inventory
            deserializedInventory.setItem(stackPosition, is);
        }

        //Return the inventory!
        return deserializedInventory;
    }

    //==================================================================================================================
}
