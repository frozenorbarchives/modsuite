package me.chollasch.plugins.qmodsuite.inventory;

import com.google.common.collect.Lists;
import me.chollasch.plugins.qmodsuite.Config;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Created by Connor Hollasch on 9/30/14.
 */

public class StaffItems {

    public static ItemStack PASSTHROUGH;
    public static ItemStack PGUI_BOOK;
    public static ItemStack WORLDEDIT;
    public static ItemStack RECORDING_ON;
    public static ItemStack RECORDING_OFF;
    public static ItemStack RANDOM_TELEPORT;

    public static void load() {
        PASSTHROUGH = build(Material.COMPASS, name("ITEMS.COMPASS-PASSTHROUGH.NAME"), lore("ITEMS.COMPASS-PASSTHROUGH.LORE"));
        PGUI_BOOK = build(Material.BOOK, name("ITEMS.INVENTORY-VIEWER-BOOK.NAME"), lore("ITEMS.INVENTORY-VIEWER-BOOK.LORE"));
        WORLDEDIT = build(Material.WOOD_AXE, name("ITEMS.WORLD-EDIT-WAND.NAME"), lore("ITEMS.WORLD-EDIT-WAND.LORE"));
        RECORDING_ON = build(Material.CARPET, (byte) 14, name("ITEMS.RECORDING-ON.NAME"), lore("ITEMS.RECORDING-ON.LORE"));
        RECORDING_OFF = build(Material.CARPET, name("ITEMS.RECORDING-OFF.NAME"), lore("ITEMS.RECORDING-OFF.LORE"));
        RANDOM_TELEPORT = build(Material.getMaterial(2258), name("ITEMS.RANDOM-TELEPORT.NAME"), lore("ITEMS.RANDOM-TELEPORT.LORE"));
    }

    private static FileConfiguration conf;

    private static String name(String section) {
        if (conf == null)
            conf = Config.get();

        return ChatColor.translateAlternateColorCodes('&', conf.getString(section));
    }

    private static String[] lore(String section) {
        if (conf == null)
            conf = Config.get();

        List<String> empty = Lists.newArrayList();
        if (conf.contains(section)) {
            empty = conf.getStringList(section);
        }

        return empty.toArray(new String[0]);
    }

    public static ItemStack build(Material type, String displayName, String... lore) {
        return build(type, 1, (byte)0, displayName, lore);
    }

    public static ItemStack build(Material type, int amount, byte data, String displayName, String... lore) {
        ItemStack stack = new ItemStack(type, amount, (short)1, data);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
        if (lore.length > 0) {
            List<String> l = Lists.newArrayList();
            for (String s : lore) {
                l.add(ChatColor.translateAlternateColorCodes('&', s));
            }
            meta.setLore(l);
        }
        stack.setItemMeta(meta);
        return stack;
    }

    public static ItemStack build(Material type, int amount, String displayName, String... lore) {
        return build(type, amount, (byte)0, displayName, lore);
    }

    public static ItemStack build(Material type, byte data, String displayName, String... lore) {
        return build(type, 1, data, displayName, lore);
    }
}
