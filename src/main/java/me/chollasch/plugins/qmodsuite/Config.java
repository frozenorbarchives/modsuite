package me.chollasch.plugins.qmodsuite;

import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by Connor Hollasch on 9/29/14. Test.
 */

//Almost seems useless, just an easy way to access the config!
public class Config {

    /**
     * Get the config instance.
     *
     * @return FileConfiguration from the JavaPlugin getConfig() method
     */
    public static FileConfiguration get() {
        return QModSuite.getInstance().getConfig();
    }

}
