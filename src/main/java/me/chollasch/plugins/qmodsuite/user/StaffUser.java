package me.chollasch.plugins.qmodsuite.user;

import me.chollasch.plugins.qmodsuite.Config;
import me.chollasch.plugins.qmodsuite.QModSuite;
import me.chollasch.plugins.qmodsuite.inventory.InventoryLogger;
import me.chollasch.plugins.qmodsuite.inventory.StaffItems;
import me.chollasch.plugins.qmodsuite.task.RecordingTask;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Connor Hollasch on 9/29/14.
 */

public class StaffUser {

    //==================================================================================================================

    /** */
    private static HashMap<Player, StaffUser> staffUsers = new HashMap<>();

    public static StaffUser getUserFromPlayer(Player player) {
        return staffUsers.get(player);
    }

    //==================================================================================================================

    /** Hold the player object */
    private Player player;

    /** If the player is in staff mode */
    private boolean isStaffMode = true;

    //==================================================================================================================

    /**
     * Create a staff user object with the given player
     *
     * @param player player as staff user
     */
    public StaffUser(Player player) {
        this.player = player;

        //Add them to the map so we can access them statically
        staffUsers.put(player, this);
    }

    //==================================================================================================================

    public void setIsStaffMode(boolean staffMode) {
        this.isStaffMode = staffMode;

        //Update staff mode and catch IOException
        try { updateStaffMode(); } catch (IOException ex) { ex.printStackTrace(); }
    }

    public boolean isStaffMode() {
        return isStaffMode;
    }

    public Player getPlayer() {
        return player;
    }

    //==================================================================================================================

    public void updateStaffMode() throws IOException {
        if (isStaffMode) {
            //Make sure we hide the player!
            QModSuite.getHiddenPlayers().add(player);

            //Hide the player from everyone else
            for (Player other : Bukkit.getOnlinePlayers()) {
                player.showPlayer(other);

                //Check if the other player has permission for vanish, if they do, ignore them (or if they're recording)
                if (other.hasPermission(Config.get().getString("DEFAULTS.PERMISSION.STAFF-MODE")) && !(RecordingTask.isRecording(other)))
                    continue;

                other.hidePlayer(player);
            }

            //Save the users data
            InventoryLogger.savePlayerData(player);

            //Clear the players inventory
            player.getInventory().clear();

            //Give player moderation tools
            player.getInventory().setItem(0, StaffItems.PASSTHROUGH);
            player.getInventory().setItem(1, StaffItems.PGUI_BOOK);
            player.getInventory().setItem(2, StaffItems.WORLDEDIT);
            player.getInventory().setItem(3, StaffItems.RECORDING_OFF);
            player.getInventory().setItem(8, StaffItems.RANDOM_TELEPORT);

            //Update their inventory
            player.updateInventory();
        } else {
            InventoryLogger.loadPlayerData(player);

            //Make sure the player is visible to everyone!
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.showPlayer(player);
            }
        }
    }

    //==================================================================================================================
}
